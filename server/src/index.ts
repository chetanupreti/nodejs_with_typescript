import express from "express";
import cookieSesion from "cookie-session"
const path = require('path');

import { AppRouter } from "./AppRouter";

import "./controllers/loginController";
import "./controllers/rootController";
const app = express();

// Static Files
app.use(express.static('public'));
app.use('/*/css', express.static(__dirname + '/public/css'));
app.use('/*/js', express.static(__dirname + '/public/js'));
app.use('/*/img', express.static(__dirname + '/public/img'));

// Set View's
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(cookieSesion({ keys: ['chetan'] }));
app.use(AppRouter.getInstance());

app.get('/', (req, res) => {
        res.redirect('/auth/login');
})
const PORT = process.env.port || 5000;

app.listen(PORT, () => {
        console.log(`server is listening on port ${PORT}`);
})

