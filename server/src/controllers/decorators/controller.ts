import 'reflect-metadata';
import { AppRouter } from "../../AppRouter";
import { Method } from "./Method";
import { MetadataKeys } from "./MetadataKeys";
import {Request, Response,NextFunction, RequestHandler } from 'express';


function bodyValidators(keys:string[]):RequestHandler {
    return function(req:Request,res:Response,next:NextFunction){
        if(!req.body){
            res.status(422).send('Invalid Request');
            return;
        }

        for(let key of keys) {
            if(!req.body[key]){
                res.status(422).send(`Body Parameter ${key} is missing`);
                return;
            }
        }
        next();
    }
}

export function controller(routePrefix:string) {
    return function(target:Function){
        const router = AppRouter.getInstance();
        for(let key in target.prototype){
            const routeHanlder = target.prototype[key];
            const path = Reflect.getMetadata(MetadataKeys.path,target.prototype,key);
            const method:Method = Reflect.getMetadata(MetadataKeys.method,target.prototype,key);
            const middleWares = Reflect.getMetadata(MetadataKeys.middleware,target.prototype,key) ||[];
            const requiredProps = Reflect.getMetadata(MetadataKeys.validator,target.prototype,key) ||[];

            const validator = bodyValidators(requiredProps);

            if(path){
                router[method](`${routePrefix}${path}`,...middleWares,validator,routeHanlder);
            }
        }
    }
}