import { NextFunction, Request, Response } from "express";
import { get,post,controller,use,bodyValidator } from "./decorators";
const path = require('path');

function logger(req:Request,res:Response,next:NextFunction){
	console.log('in logger');
	next();
}


@controller('/auth')
class LoginController {
	
	@get('/login')
	@use(logger)
	getLogin(req: Request, res: Response) {
		res.render('login');
	}

	@post('/login')
	@bodyValidator('email','password')
	postLogin(req:Request,res:Response){
			const {email,password} = req.body;
			if(email==='chetan@gmail.com' && password==='chetan') {
				req.session = {loogedIn:true};
				res.redirect('/auth/protected');
			}else {
				res.send('<h1>Invalid email and password</h1>');
			}
		}

		@get('/logout')
		getLogout(req:Request,res:Response){
			req.session = null;
			res.redirect('/auth');
		}
}