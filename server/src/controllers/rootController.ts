import {  Request,Response, NextFunction} from "express";

import { get,controller,use } from "./decorators";


function requireAuth(req:Request,res:Response,next:NextFunction):void{
    if(req.session && req.session.loogedIn){
        next();
        return;
    }

    res.status(403);
    res.render('forbidden');
}

@controller('/auth')
class RootController {

    @get('/')
    getRoot(req:Request,res:Response){
        if(req.session && req.session.loogedIn) {
            res.send(
                    `<div>
                        <div><h1>You are looged in</h1></div>
                        <a href="/auth/logout"><h3>Logout</h3></a>
                    </div>
                    `
                )
        } else {
            res.send(
                    `<div>
                        <div><h1>You are not looged in</h1></div>
                        <a href="/auth/login"><h3>Login</h3></a>
                        <a href="/auth/protected"><h3>Protected</h3></a>
                    </div>`
                )
        } 
    }
    
    @get('/protected')
    @use(requireAuth)
    getProtected(req:Request,res:Response){
        res.send('<h1>Welcome to protected route,logged in user</h1> <a href="/auth/logout"><h3>Logout</h3></a>');
    }
}