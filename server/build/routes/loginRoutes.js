"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.router = void 0;
var express_1 = require("express");
var router = express_1.Router();
exports.router = router;
router.post('/login', function (req, res) {
    var _a = req.body, email = _a.email, password = _a.password;
    if (email && password && email === 'chetan@gmail.com' && password === 'chetan') {
        req.session = { loogedIn: true };
        res.redirect('/');
    }
    else {
        res.send('Invalid email and password');
    }
});
router.get('/', function (req, res) {
    if (req.session && req.session.loogedIn) {
        res.send("\n            <div>\n            <div>You are looged in</div>\n            <a href=\"/logout\">Logout</a>\n            </div>\n            ");
    }
    else {
        res.send("\n            <div>\n            <div>You are not looged in</div>\n            <a href=\"/login\">Login</a>\n            </div>\n            ");
    }
});
router.get('/logout', function (req, res) {
    req.session = null;
    res.redirect('/');
});
router.get('/protected', requireAuth, function (req, res) {
    res.send('Welcome to protected route,logged in user');
});
