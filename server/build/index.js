"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var cookie_session_1 = __importDefault(require("cookie-session"));
var path = require('path');
var AppRouter_1 = require("./AppRouter");
require("./controllers/loginController");
require("./controllers/rootController");
var app = express_1.default();
// Static Files
app.use(express_1.default.static('public'));
app.use('/*/css', express_1.default.static(__dirname + '/public/css'));
app.use('/*/js', express_1.default.static(__dirname + '/public/js'));
app.use('/*/img', express_1.default.static(__dirname + '/public/img'));
// Set View's
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express_1.default.json());
app.use(express_1.default.urlencoded({ extended: true }));
app.use(cookie_session_1.default({ keys: ['chetan'] }));
app.use(AppRouter_1.AppRouter.getInstance());
app.get('/', function (req, res) {
    res.redirect('/auth/login');
});
var PORT = process.env.port || 5000;
app.listen(PORT, function () {
    console.log("server is listening on port " + PORT);
});
